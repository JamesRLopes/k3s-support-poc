# Deploying GitLab in Helm for Support using self-hosted infrastructure

### Disclaimers
1. The content is specifically designed for support engineers to use to stand up a [Proof of Concept](https://docs.gitlab.com/charts/installation/deployment.html) deployment of GitLab in Kubernetes. If you're using this for other purposes the information may not be relevant and support will not be able to assist you with your deployment. **This guide shgould not be used for production deployments or customer use**.
2. This guide works as of January 2024. Future releases may break compatability.
3. This deploys a GitLab PoC install into the default namespace.

### Pre-requisites

1. Ideally have a Top-Level-Domain with DNS control that you can point to your kubernetes infrastructure. It's not a requirement and local DNS can be used but it is outside the scope of this guide.

1. Have an Ubuntu (or other Debian derivative) Linux based system with at least 12GB of RAM available, 16 GB is preferred but I have deployed on a VM with 12GB - though a few pods competed for resources and had to be restarted strategically.
    - Old refreshed hardware is perfect for this!
    - I recommend standing up a VM instead of running on baremetal for ease of snapshotting, wiping, and restarting. This guide was tested using Ubuntu 22.04 in a virtualbox VM
      - Networking for the VM should be done in bridged mode since you need a routable address from your local computer to the VM.

1. Have helm and kubectl installed on your local pc.
   - Install helm: https://helm.sh/docs/intro/install/
   - Install kubectl: https://kubernetes.io/docs/tasks/tools/#kubectl
     - I recommend aliasing `k = kubectl` see [the notes](https://kubernetes.io/docs/reference/kubectl/quick-reference/#kubectl-autocomplete)

1. Optional but recommended TLS support:  have an Internet-accessible domain for which you control the DNS zone:
    - Install Certbot: https://certbot.eff.org/
    - Provision a Let's Encrypt certificate:
```
certbot certonly --manual --preferred-challenges dns-01 --email you@example.com --config-dir . --work-dir . --logs-dir . --server https://acme-v02.api.letsencrypt.org/directory -d '*.k3s.example.com,k3s.example.com'
```


### Install K3s as your kubernetes infrastructure

1. Install K3s following the quickstart guide: https://docs.k3s.io/quick-start

2. Disable traefik ingress: https://docs.k3s.io/networking#traefik-ingress-controller
   - I usually just edit `/etc/systemd/system/k3s.service` and add this flag before reloading and restarting k3s:
   ```
   ExecStart=/usr/local/bin/k3s \
    server --disable=traefik
    ```

3. Copy the `/etc/rancher/k3s/k3s.yaml` file from your kubernetes server to your local computer under `~/.kube/config.k3s` (path may differ on other distros). Modify the file to change the `server` definition to the internal IP address of your kubernetes server instead of `127.0.0.1`

4. Run `kubectl get nodes` and observe the following output if you've successfully installed kubernetes:
```
$ kubectl get nodes
NAME   STATUS   ROLES                  AGE   VERSION
kubeVM    Ready    control-plane,master   10m   v1.28.5+k3s1
```

5. If you created your TLS certificate manually above, create a secret to make it available to your GitLab installation:
```
$ cd to the directory where Certbot wrote your certificate chain and private key
$ kubectl create secret tls <your_TLS_secret_name> --cert=./fullchain.pem --key=./privkey.pem
```

### Deploy GitLab using helm

1. Determine your external IP and base domain
1. Add the Gitlab repo and update
    ```
    helm repo add gitlab https://charts.gitlab.io/
    helm repo update
    ```
1. Create a minimal base values configuration. Example `gitlab.yaml`:
```
kas:
  enabled: false
global:
  rails:
    bootsnap:
      enabled: false
  shell:
    port: 32022   #change shell port to avoid conflict with host sshd
prometheus:
  install: false
gitlab-runner:
  install: false
# Reduce replica counts, reducing CPU & memory requirements
gitlab:
  webservice:
    minReplicas: 1
    maxReplicas: 1
  sidekiq:
    minReplicas: 1
    maxReplicas: 1
  gitlab-shell:
    minReplicas: 1
    maxReplicas: 1
registry:
  hpa:
    minReplicas: 1
    maxReplicas: 1
```
4. Install the GitLab chart specifying your gitlab.yaml values:

- `global.hosts.domain` should be the top level domain for your kubernetes infra. ie k3s.domain.com. By default it will setup the following:
  - gitlab.k3s.domain.com
  - minio.k3s.domain.com
  - registry.k3s.domain.com
  - kas.k3s.domain.com
- `global.hosts.externalIP` should be the external IP of your K3s server.
- `certmanager-issuer.email` is used by CertManager when requesting TLS certificates for GitLab Ingresses. Just put your gitlab or personal email here.
- `version` defines which version of GitLab to install. See the [list here](https://docs.gitlab.com/charts/installation/version_mappings.html#previous-chart-versions) of the chart mapping to GL version mapping

```
helm upgrade --install gitlab gitlab/gitlab \
--timeout 600s \ 
--set global.hosts.domain=k3s.example.com \
--set global.hosts.externalIP= \
--set certmanager-issuer.email= \
--set postgresql.image.tag=13.6.0 \
--version=7.1.5 \
-f gitlab.yaml
```

If you provisioned your TLS certificate manually with Certbot, use this syntax:

```
helm upgrade --install gitlab gitlab/gitlab \
--timeout 600s \ 
--set global.hosts.domain=k3s.example.com \
--set global.hosts.externalIP= \
--set certmanager.install=false \
--set global.ingress.configureCertmanager=false \
--set global.ingress.tls.secretName=<your_TLS_secret_name> \
--set postgresql.image.tag=13.6.0 \
--version=7.1.5 \
-f gitlab.yaml
```

5. Observe the pods spinning up with `kubectl get pods` or the [K9s](https://k9scli.io/) utility

6. Once the webservice pods are in a `READY` state check the UI in your browser: `https://gitlab.<hosts.domain>` 

7. You can get the initial root password by decoding the secret:
- `kubectl get secrets  gitlab-gitlab-initial-root-password -ojsonpath='{.data.password}' | base64 -d; echo`
